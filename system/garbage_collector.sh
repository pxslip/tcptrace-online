#! /bin/bash
#
# HK April 2014
# find directories older than some time value, and operate on them (presumably archive or delete)
# the script protects directories in which files where accessed more recently than the cutoff.
#
# specify how old a diretory should be to be considered for deletion (m=minutes, h=hours, d=days)
CUTOFF="1h"
# top of the directory tree to search from
SEARCH_START="/Users/hkruse/tmp"
# use a non-blank entry to limit the directory names to start with this prefix
SEARCH_PREFIX="dir"
# the actual cleanup command.  Set to listing the directories for now. For production (TEST FIRST!):
# COMMAND="rm -Rf"
# 
COMMAND="ls -lcd"
#
# protect_dir is called for every candidate (old) directory.  Any file that has been 
# accessed within the cutoff time will reset the directory modification time to now.
# That has the side-effect that directories could stick around as long as twice the cutoff value.
function protect_dir() {
	echo "IN: "+$1
	FILELIST=`find $1 -type f -atime -${CUTOFF} -print`
	for FILE in $FILELIST
	do
		echo "Using: "+$FILE
		touch $1
	done
}
#
# find all directories older than the cutoff (in modification time)
# scan for files with access time less than the cutoff and reset those directories
#
DIRLIST=`find $SEARCH_START -type d -mtime +${CUTOFF} -name "${SEARCH_PREFIX}*" -print`
for DIRNAME in $DIRLIST
do
	protect_dir $DIRNAME
done
#
# operate on all directories that are still older (in modification time) than the cutoff.
#
find $SEARCH_START -type d -mtime +${CUTOFF} -name ${SEARCH_PREFIX}* -print0 | xargs -0 $COMMAND