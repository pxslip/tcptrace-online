'use strict';
/*
 * App Dependencies
 */
var sys = require('sys');
var exec = require('child_process').exec;
var fs = require('fs');
var uuid = require('node-uuid');
var formidable = require('formidable');
var _ = require('lodash');
var mkdirp = require('mkdirp');

/*
 * Express Dependencies
 */
var express = require('express');
var app = express();
var port = 3000;

/*
 * Use Handlebars for templating
 */
var exphbs = require('express3-handlebars');
var hbs;

// For gzip compression
app.use(express.compress());
//set up the session
app.use(express.cookieParser());
app.use(express.session({secret: 'xplot is a pain!'}));
app.use(express.json());
app.use(express.urlencoded());
/*
 * Config for Production and Development
 */

if (process.env.NODE_ENV === 'production') {
	// Set the default layout and locate layouts and partials
	app.engine('handlebars', exphbs({
		defaultLayout: 'main',
		layoutsDir: 'dist/views/layouts/',
		partialsDir: 'dist/views/partials/'
	}));

	// Locate the views
	app.set('views', __dirname + '/dist/views');
	
	// Locate the assets
	app.use(express.static(__dirname + '/dist/assets'));

} else {
	app.engine('handlebars', exphbs({
		// Default Layout and locate layouts and partials
		defaultLayout: 'main',
		layoutsDir: 'views/layouts/',
		partialsDir: 'views/partials/'
	}));

	// Locate the views
	app.set('views', __dirname + '/views');
	
	// Locate the assets
	app.use(express.static(__dirname + '/assets'));
}

// Set Handlebars
app.set('view engine', 'handlebars');


/*
 * Routes
 */
// Index Page
app.get('/', function(request, response, next) {
	response.render('index');
});
// Handler to receive a file and run the short summary, parse it and then run long for each connection
app.post('/upload', function(request, response, next) {
	var form, path, tag;
	form = new formidable.IncomingForm();
	form.parse(request, function(error, fields, files) {
		//first create the working directory for this upload
		request.session.tag = tag = uuid.v1();//use a nice long random string for this session
		path = '/tmp/tcpto/'+tag;
		mkdirp.sync(path);
		//move the file to the directory we just created
		fs.renameSync(files.dumpFile.path, path+'/dumpFile');
		exec('tcptrace '+path+'/dumpFile', function(error, stdout, stderr) {
			var connArrayTemp = _.compact(stdout.split('\n'));
			var diagArray = connArrayTemp.splice(0, 6);
			var connArray = [];
			for (var i = 0; i < connArrayTemp.length; i++) {//split each conn string into a more usable array
				var conn = /\s*(\d+?):\s+(.+?)\s+-\s+(.+?)\s+(.+)/.exec(connArrayTemp[i]);
				conn.splice(0,1);
				connArray.push({id:conn[0], hosta:conn[1], hostb:conn[2], info:conn[3]});
			};
			response.send({prefix:diagArray, connections:connArray});
		});
	});
});
//this handles the request for the long format info for a given connection
app.post('/connection-info', function(request, response, next) {
	var tag, path, lineArrayTemp, lines = [], i, line, connectionNames, headerArray, connNumber, lineRegex;
	//get the tag from session
	tag = request.session.tag;
	//get the path to the correct working directory
	path = '/tmp/tcpto/'+tag;
	exec('tcptrace -l -o'+request.body.id+' '+path+'/dumpFile', function(error, stdout, stderr) {
		lineArrayTemp = _.compact(stdout.split('\n'));
		if(lineArrayTemp[7] === '================================') {
			lineArrayTemp.splice(0,8);
		} else {
			lineArrayTemp.splice(0,7);
		}
		lineArrayTemp.splice(8,1);
		headerArray = lineArrayTemp.splice(0,8);
		connNumber = headerArray.shift();
		connectionNames = /(.+?):\s+(.+?):/.exec(lineArrayTemp.splice(0,1)[0].trim());
		connectionNames.splice(0,1);
		lineRegex = /(.+?):\s+(.+?)\s+(.+?):\s+(.+)/
		for(i = 0; i < lineArrayTemp.length; i++) {//handle the header code
			line = lineArrayTemp[i].trim();
			line = lineRegex.exec(line);
			lines.push({a:{label:line[1], value:line[2]}, b:{label:line[3], value:line[4]}});
		}
		response.send({lines: lines, header: headerArray, names: connectionNames, number:connNumber});
	});
});

app.post('/graph', function(request, response, next) {
	var graphParams = '-o'+request.body.id+' ',
		graphType = request.body.graphType,
		color = request.body.graphColor,
		zeroXAxis = request.body.zeroXAxis,
		zeroYAxis = request.body.zeroYAxis,
		path = '/tmp/tcpto/'+request.session.tag,
		cwd = process.cwd(),
		graphSuffix;
		console.log(request.body);
	if(zeroYAxis&&zeroXAxis) {
		graphParams += '-zxy ';
	} else if(zeroXAxis) {
		graphParams += '-zx ';
	} else if(zeroXAxis) {
		graphParams += '-zy ';
	}
	if(!color) {
		graphParams += '-M ';
	}
	process.chdir(path);
	switch(graphType) {
		case 'time_sequence':
			graphParams += '-S ';
			graphSuffix = 'tsg';
			break;
		case 'throughput':
			graphParams += '-T ';
			graphSuffix = 'tput';
			break;
		case 'rtt':
			graphParams += '-R ';
			graphSuffix = 'rtt';
			break;
		case 'outstanding_data':
			graphParams += '-N ';
			graphSuffix = 'owin';
			break;
		case 'segment_size':
			graphParams += '-F ';
			graphSuffix = 'ssize';
			break;
	};
	exec('tcptrace '+graphParams+'dumpFile', function(error, stdout, stderr) {
		//this should get the connection lettering (i.e. a2b or y2z)
		var connInfo = /\s+.+?\s.+ - .+ \((.+?)\).+/.exec(_.compact(stdout.split('\n')).pop()),
			connNameA = connInfo[1],
			connNameB = connNameA.split('2').reverse().join('2'),
			graphA, graphB;
		try {
			graphA = fs.readFileSync(connNameA+'_'+graphSuffix+'.xpl', 'utf8');
		} catch(exc) {
			graphA = null;
		}
		try {
			graphB = fs.readFileSync(connNameB+'_'+graphSuffix+'.xpl', 'utf8');
		} catch(exc) {
			graphB = null;
		}
		process.chdir(cwd);
		response.send({graphA:graphA, graphB:graphB});
	});
});

app.get('/session-exists', function(request, response, next) {
	if(request.session.tag) {
		var path = '/tmp/tcpto/'+request.session.tag;
		if(fs.existsSync(path)) {
			exec('tcptrace '+path+'/dumpFile', function(error, stdout, stderr) {
				var connArrayTemp = _.compact(stdout.split('\n'));
				var diagArray = connArrayTemp.splice(0, 6);
				var connArray = [];
				for (var i = 0; i < connArrayTemp.length; i++) {//split each conn string into a more usable array
					var conn = /\s*(\d+?):\s+(.+?)\s+-\s+(.+?)\s+(.+)/.exec(connArrayTemp[i]);
					conn.splice(0,1);
					connArray.push({id:conn[0], hosta:conn[1], hostb:conn[2], info:conn[3]});
				};
				response.send({session:true, prefix:diagArray, connections:connArray});
			});
		} else {
			response.send({session:false});
		}
	} else {
		response.send({session:false});
	}
});
/*
 * Start it up
 */
app.listen(process.env.PORT || port);
console.log('Express started on port ' + port);