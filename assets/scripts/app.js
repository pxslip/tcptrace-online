/*global angular*/
var tcpto = angular.module('tcpto', ['ngRoute', 'angularFileUpload', 'mgcrea.ngStrap.affix', 'ui.bootstrap']);
tcpto.config(function ($routeProvider) {
	'use strict';
	$routeProvider
		.when('/', {
			controller: 'MainCtrl',
			templateUrl: '/templates/main.html'
		})
		.when('/output', {
			controller: 'OutputCtrl',
			templateUrl: '/templates/output.html'
		});
});
tcpto.controller('MainCtrl', function ($scope, $fileUploader, $http) {
	'use strict';
	$scope.displayProgress = false;
	$scope.displayProcessing = false;
	$scope.loading = 'loading';
	$scope.tabs = {summary:false};
	$scope.graphTypes = ['throughput', 'time_sequence', 'rtt', 'outstanding_data', 'segment_size', 'timeline'];
	var uploader = $scope.uploader = $fileUploader.create({
		scope: $scope,
		url: '/upload',
		alias: 'dumpFile',
		autoUpload: true
	});
	uploader.bind('success', function (evt, xhr, item, response) {
		$scope.displayProcessing = false;
		//handle the successful upload's returned data
		$scope.connections = response.connections;
	});
	uploader.bind('progress', function (evt, item, progress) {
		if (progress === 100) {
			$scope.displayProgress = false;
			$scope.displayProcessing = true;
		}
	});
	uploader.bind('beforeupload', function (event, item) {
		$scope.displayProgress = true;
		console.log('test');
	});
	$scope.connectionClick = function (id) {
		$scope.connectionId = id;
		$scope.tabs.summary = true;
		$http.post('/connection-info', {id: id})
			.success(function (data, status, headers, config) {
				$scope.connInfoHeader = data.header;
				$scope.connInfoLines = data.lines;
				$scope.connInfoNames = data.names;
				$scope.connInfoNumber = data.number;
			})
			.error(function (data, status, headers, config) {
				console.log(data);
			});
	};
	$scope.graphClick = function(type) {
		$http.post('/graph',
			{id:$scope.connectionId, graphType:type, graphColor:$scope.graphColor, zeroXAxis:$scope.graphZeroX, zeroYAxis:$scope.graphZeroY})
			.success(function (data, status, headers, config) {
				$scope.graphA = data.graphA;
				$scope.graphB = data.graphB;
				$scope.activeGraph = ($scope.graphA) ? 'A' : 'B';
				$scope.graphType = type;
				if($scope.graphA || $scope.graphB) {
					CanvasHandler.init(type+'_plot', ($scope.graphA) ? $scope.graphA : $scope.graphB);
				}
			})
			.error(function (data, status, headers, config) {
				console.log(data);
			});
	};
	$scope.flipGraphDirection = function () {
		if($scope.graphA && $scope.graphB) {
			$scope.activeGraph = ($scope.activeGraph === 'A') ? 'B' : 'A';
			CanvasHandler.init($scope.graphType+'_plot', $scope['graph'+$scope.activeGraph]);
		}
	};
	$http.get('/session-exists')
		.success(function (data, status, headers, config) {
			if(data.session) {
				$scope.connections = data.connections;
				$scope.loading = 'done-loading';
			} else {
				$scope.loading = 'done-loading';
			}
		});
	$scope.graphColor = true;
	$scope.graphZeroX = true;
	$scope.graphZeroY = true;
});
tcpto.directive('tcptoConnectionList', [function () {
	'use strict';
	return {
		templateUrl: 'partials/connection-list.html'
	};
}]);
tcpto.directive('tcptoConnectionInfo', [function () {
	'use strict';
	return {
		templateUrl: 'partials/connection-info.html'
	};
}]);